﻿using MySQLDemo.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool IsAdd = false;
        public MainPage()
        {
            this.InitializeComponent();
            this.Splitter.RegisterPropertyChangedCallback(SplitView.IsPaneOpenProperty, IsPaneOpenPropertyChanged);

            names.ItemsSource = loadAllTheData();
            Splitter.IsPaneOpen = true;
            menu.Orientation = Orientation.Horizontal;
           

            InfoFrame.Visibility = Visibility.Collapsed;

            //var screenSize = ScreenSize();
            //Splitter.OpenPaneLength = screenSize.Width;
        }
        private void IsPaneOpenPropertyChanged(DependencyObject sender, DependencyProperty dp)
        {
            menu.Orientation = (Splitter.IsPaneOpen == true) ? Orientation.Horizontal : Orientation.Vertical;

            if(ContactImage.Visibility == Visibility.Visible)
            {
                Thickness margin = ContactImage.Margin;
                margin.Left = (Splitter.IsPaneOpen == true) ? 4 : 50;
               
                ContactImage.Margin = margin;
                
            }
        }

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);
            var date = new DateTime();

            ContactName.Text = $"{b[1]} {b[2]}";
            userAccount.Text = b[0];
            fname.Text = b[1];
            lname.Text = b[2];

            DateTime.TryParse(b[3], out date);
            dob.Text = date.Date.ToString();

            strno.Text = b[4];
            strname.Text = b[5];
            postcode.Text = b[6];
            city.Text = b[7];
            phone1.Text = b[8];
            phone2.Text = b[9];
            email.Text = b[10];

            InfoFrame.Visibility = Visibility.Visible;
        }

        void selectedId(string id)
        {
            var command = $"Select * from tbl_people WHERE ID = '{id}' ";
            var b = MySQLCustom.ShowInList(command);
            var date = new DateTime();
            ContactName.Text = $"{b[1]} {b[2]}";
            userAccount.Text = b[0];
            fname.Text = b[1];
            lname.Text = b[2];

            DateTime.TryParse(b[3], out date);

            dob.Text = date.Date.ToString();
            strno.Text = b[4];
            strname.Text = b[5];
            postcode.Text = b[6];
            city.Text = b[7];
            phone1.Text = b[8];
            phone2.Text = b[9];
            email.Text = b[10];

            InfoFrame.Visibility = Visibility.Visible;

            ResetToggles();
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        void clearAll()
        {
            fname.Text = "";
            lname.Text = "";
            dob.Text = "";
            email.Text = "";
            phone1.Text = "";
            phone2.Text = "";
            userAccount.Text = "";
            strname.Text = "";
            postcode.Text = "";
            strno.Text = "";
            city.Text = "";
            ContactName.Text = "";

        }

        void ResetAll()
        {
            clearAll();

            Add.Visibility = Visibility.Visible;

            Save.Visibility = Visibility.Collapsed;
            Cancel.Visibility = Visibility.Collapsed;

            Edit.Visibility = Visibility.Visible;
            Delete.Visibility = Visibility.Visible;

            InfoFrame.Visibility = Visibility.Collapsed;
            Splitter.IsPaneOpen = true;

        }

        private Size ScreenSize()
        {
            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var size = new Size(bounds.Width * scaleFactor, bounds.Height * scaleFactor);
            return size;
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Checking value";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            {
                return;
            }
        }

        //Below are the object methods

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {                
                selectedName(names.SelectedItem.ToString());
                InfoFrame.Visibility = Visibility.Visible;
                Delete.Visibility = Visibility.Visible;
                Edit.Visibility = Visibility.Visible;
                var size = ScreenSize();
                if(size.Width < 721)
                {
                    Splitter.IsPaneOpen = false;
                }                
                Controls();
            }
            else
            {
                InfoFrame.Visibility = Visibility.Collapsed;
                Delete.Visibility = Visibility.Collapsed;
                Edit.Visibility = Visibility.Collapsed;
                
            }
        }

        private void addToDB()
        {
            if(fname.Text.Trim() == string.Empty)
            {
                messageBox("Contact must have first name");
                return;
            }
            //Add Information to the Database
            MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, strno.Text, strname.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            names.ItemsSource = loadAllTheData();
        }

        private void updateDB()
        {
            if (fname.Text.Trim() == string.Empty)
            {
                messageBox("Contact must have first name");
                return;
            }
            //Update Information in the Database
            MySQLCustom.UpdateData(fname.Text, lname.Text, dob.Text, strno.Text, strname.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text, userAccount.Text);
            names.ItemsSource = loadAllTheData();
        }

        private void removeFromDB()
        {
          //  Remove Information from the Database
            MySQLCustom.DeleteDate(userAccount.Text);
            names.ItemsSource = loadAllTheData();
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = (Splitter.IsPaneOpen == true) ? false : true;          

            StatusBorder.Visibility = Visibility.Collapsed;
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = false;

            ContactName.Text = "New Contact";
            StatusBorder.Visibility = Visibility.Collapsed;

            InfoFrame.Visibility = Visibility.Visible;
            IsAdd = true;

            Add.Visibility = Visibility.Collapsed;
            Edit.Visibility = Visibility.Collapsed;
            Delete.Visibility = Visibility.Collapsed;

            clearAll();

            Controls();
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            InfoFrame.IsEnabled = true;
            
            Edit.Visibility = Visibility.Collapsed;
            Delete.Visibility = Visibility.Collapsed;

            Controls();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            removeFromDB();
            StatusLabel.Text = "Delete was successful";
            ResetAll();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if(fname.Text.Trim() == string.Empty)
            {
                messageBox($"Unable to save {fname.Text} has no value");
                return;
            }

            if (dob.Text.Trim() != string.Empty)
            {                
                DateTime birth;

                if(!DateTime.TryParse(dob.Text, out birth))
                {
                    messageBox($"Unable to save {dob.Text} as its not a date");
                    return;
                }
            }

            if (IsAdd)
            {
                if (names.Items.Contains(fname.Text.Trim()))
                {
                    messageBox($"Unable to save {fname.Text} as this name exists");
                    return;
                }
                addToDB();
                StatusLabel.Text = "Add was successful";
                selectedName(fname.Text);
                ResetToggles();
            }
            else
            {
                updateDB();
                StatusLabel.Text = "Update was successful";
                selectedId(userAccount.Text);
                ResetToggles();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if(IsAdd)
            {
                ResetAll();
            }
            else
            {
                selectedId(userAccount.Text);
                ResetToggles();                
            }
        }

        private void ResetToggles()
        {
            Add.Visibility = Visibility.Visible;
            
            Edit.Visibility = Visibility.Visible;
            Delete.Visibility = Visibility.Visible;
            Controls();
        }

        private void Controls()
        {
            fname.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            lname.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            dob.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            phone1.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            phone2.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            email.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            strno.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            strname.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            postcode.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;
            city.IsEnabled = (Edit.Visibility == Visibility.Visible) ? false : true;

            Save.Visibility = (Edit.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
            Cancel.Visibility = (Edit.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;

            StatusLabel.Text = "";
        }

        
    }
}

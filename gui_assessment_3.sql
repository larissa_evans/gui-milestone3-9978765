-- Preset Stuff

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- Creating the Database

CREATE DATABASE IF NOT EXISTS `gui_comp6001_16b_assn3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gui_comp6001_16b_assn3`;

-- Drop Table if it exists, this is great for when you are restoring a DB to default settings
DROP TABLE IF EXISTS `tbl_people`;

-- Create the table(s)
CREATE TABLE `tbl_people` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FNAME` varchar(30) NOT NULL,
  `LNAME` varchar(50) NOT NULL,
  `DOB` date, 
  `Str_NUMBER` int(4),
  `Str_NAME` varchar(50),
  `POSTCODE` varchar(4),
  `CITY` varchar(20),
  `PHONE1` varchar(12),
  `PHONE2` varchar(12),
  `EMAIL` varchar(50),
PRIMARY KEY (ID)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

